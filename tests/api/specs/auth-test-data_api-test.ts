import {AuthController} from "../lib/controllers/auth.controller";
import {checkStatusCode, checkResponseBodyStatus, checkResponseBodyMessage, checkResponseTime} from '../../helpers/functionsForChecking.helper';

const authController = new AuthController();

describe('Using test data to verify authorization', () => {
    let invalidCredentialsDataSet = [
        { email: 'tania.furcha.97@gmail.com', password: '' },
        { email: 'tania.furcha.97@gmail.com ', password: 'testtest97' },
        { email: ' tania.furcha.97@gmail.com', password: '*********' },
        { email: 'tania.furcha.97@gmail.co', password: 'testing' },
        { email: 'taniafurcha.97@gmail.com', password: '' },
        { email: '', password: 'lytvynuk97' },
        { email: 'Tania.Furcha@gmail.com', password: 'lytvynuk97.' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`login using invalid credentials : ${credentials.email} + ${credentials.password}`, async () => {
            let response = await authController.authenticateUser(credentials.email, credentials.password);

            checkStatusCode(response, 401);
            checkResponseBodyStatus(response, 'UNAUTHORIZED');
            checkResponseBodyMessage(response, 'Bad credentials');
            checkResponseTime(response, 3000);
        });
    });
});

/*
import {AuthController} from "../lib/controllers/auth.controller";
import {checkStatusCode, checkResponseBodyStatus, checkResponseBodyMessage, checkResponseTime} from '../../helpers/functionsForChecking.helper';

const authController = new AuthController();

describe('Without test data', () => {
    it(`login using invalid credentials email: 'tania.furcha.97@gmail.com', password: '      '`, async () => {
        let response = await authController.authenticateUser('tania.furcha.97@gmail.com', '      ');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: 'tania.furcha.97@gmail.com ', password: 'testtest97'`, async () => {
        let response = await authController.authenticateUser('tania.furcha.97@gmail.com ', 'testtest97');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: ' tania.furcha.97@gmail.com', password: '*********'`, async () => {
        let response = await authController.authenticateUser(' tania.furcha.97@gmail.com', '*********');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: 'tania.furcha.97@gmail.co', password: 'testing'`, async () => {
        let response = await authController.authenticateUser('tania.furcha.97@gmail.co', 'testing');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: 'taniafurcha.97@gmail.com', password: 'bsatest22'`, async () => {
        let response = await authController.authenticateUser('taniafurcha.97@gmail.com', 'bsatest22');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: '', password: 'lytvynuk97'`, async () => {
        let response = await authController.authenticateUser('', 'lytvynuk97');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });

    it(`login using invalid credentials email: 'Tania.Furcha@gmail.com', password: 'lytvynuk97.'`, async () => {
        let response = await authController.authenticateUser('Tania.Furcha@gmail.com', 'lytvynuk97.');

        checkStatusCode(response, 401);
        checkResponseBodyStatus(response, 'UNAUTHORIZED');
        checkResponseBodyMessage(response, 'Bad credentials');
        checkResponseTime(response, 3000);
    });
});
*/