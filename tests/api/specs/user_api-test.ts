import {AuthController} from "../lib/controllers/auth.controller";
import { checkResponseTime, checkStatusCode} from '../../helpers/functionsForChecking.helper';
import {UserController} from "../lib/controllers/user.controller";
let authController = new AuthController();
let userController = new UserController();

describe('User test', () => {
    it('test /api/user/me', async () => {
        let login = await authController.authenticateUser('tania.furcha.97@gmail.com','lytvynuk97');
        let accessToken = login.body.accessToken;
        let response = await userController.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
    });

    it('test false /api/user/me', async () => {
        let login = await authController.authenticateUser('tania.furcha.97@gmail.com','lytvynuk97');
        let accessToken = login.body.accessToken;
        let response = await userController.getCurrentUser(accessToken);
        checkStatusCode(response, 400);
        checkResponseTime(response, 200);
    });
});