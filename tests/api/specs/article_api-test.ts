import {AuthController} from "../lib/controllers/auth.controller";
import {ArticlesController} from "../lib/controllers/articles.controller";
import { checkResponseTime, checkStatusCode} from '../../helpers/functionsForChecking.helper';
let authController = new AuthController();
let articleController = new ArticlesController();


describe('Article test | with hooks', () => {
    let accessToken: string;

    before(`should get access token and userId`, async () => {
        // runs once before the first test in this block
        
        let login = await authController.authenticateUser ('tania.furcha.97@gmail.com','lytvynuk97');
        checkStatusCode(login, 200);
        accessToken = login.body.accessToken;
        
    });

    it('test /api/article/', async () => {
        let response = await articleController.saveArticle(
            accessToken,
            {
                "authorId": "1edd8662-fc94-45c8-8bd7-eaaac392879d",
                "authorName": "Test",
                "id": "92769ac6-07cf-4dea-8342-b860db932244",
                "name": "Test",
                "text": "testestestest"
            }
        );
        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
    });


    it('test /api/article/author', async () => {

        let response = await articleController.getArticles(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
    });


    it('test /api/article/article_id', async () => {

            let response = await articleController.getArticle(
                accessToken,
                '92769ac6-07cf-4dea-8342-b860db932244'
            );
            checkStatusCode(response, 200);
            checkResponseTime(response, 2000);
    });

    it('test /api/article_comment', async () => {
   
        let response = await articleController.saveComment(
            accessToken,
            {
                "articleId": "92769ac6-07cf-4dea-8342-b860db932244",
                "id": "0ec9117c-253c-45dc-ad46-b880e7f712a8",
                "text": "Tania"
            }
        );
        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
    });

    it('test /api/article_comment/of/{atricleId}?size=200', async () => {

        let response = await articleController.getComments(
            accessToken,
            '92769ac6-07cf-4dea-8342-b860db932244',
            200
        );
        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
    });

    it('test false /api/article_comment/of/{atricleId}?size=200', async () => {

        let response = await articleController.getComments(
            accessToken,
            '92769ac6-07cf-4dea-8342-b860db932244',
            200
        );
        checkStatusCode(response, 404);
        checkResponseTime(response, 2);
    });


    afterEach(function () {
        // runs after each test in this block
        console.log('It was an article test | with hooks');
    });
});