import {AuthController} from "../lib/controllers/auth.controller";
import { checkResponseTime, checkStatusCode} from '../../helpers/functionsForChecking.helper';

const authController = new AuthController();
describe('Auth test', () => {
    it('test login', async () => {
        let responseLogin = await authController.authenticateUser('tania.furcha.97@gmail.com','lytvynuk97');
        checkStatusCode(responseLogin, 200);
        checkResponseTime(responseLogin, 2000);
    });
});