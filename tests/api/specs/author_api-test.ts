import {AuthController} from "../lib/controllers/auth.controller";
import {AuthorController} from "../lib/controllers/author.controller";
import { checkResponseTime, checkStatusCode} from '../../helpers/functionsForChecking.helper';
let authController = new AuthController();
let authorController = new AuthorController();

describe('Author test', () => {
    it('test author settings GET', async () => {
        let login = await authController.authenticateUser('tania.furcha.97@gmail.com','lytvynuk97');
        let accessToken = login.body.accessToken;

        let response = await authorController.getSettings(accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
    });

    it('test author settings POST', async function () {
        let login = await authController.authenticateUser('tania.furcha.97@gmail.com','lytvynuk97');
        let accessToken = login.body.accessToken;

        let response = await authorController.setSettings(
            accessToken,
            {
                id: 'cac29cf9-d403-4c86-9c05-5439af40b75b',
                userId: '1edd8662-fc94-45c8-8bd7-eaaac392879d',
                avatar: null,
                firstName: 'Tania',
                lastName: 'Furcha',
                job: 'test',
                location: 'Ukraine',
                company: 'Test',
                website: '',
                twitter: 'https://twitter.com/',
                biography: 'test'
            }
        );
        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
    });

    it('test /api/author/overview/{userId} ', async function () {
        let login = await authController.authenticateUser('tania.furcha.97@gmail.com','lytvynuk97');
        let accessToken = login.body.accessToken;
        let response = await authorController.getPublicAuthor(
            accessToken,
            'cac29cf9-d403-4c86-9c05-5439af40b75b'
        );
        checkStatusCode(response, 200);
        checkResponseTime(response, 2000);
    });

    it('test false /api/author/overview/{userId} ', async function () {
        let login = await authController.authenticateUser('tania.furcha.97@gmail.com','lytvynuk97');
        let accessToken = login.body.accessToken;
        let response = await authorController.getPublicAuthor(
            accessToken,
            'cac29cf9-d403-4c86-9c05-5439af40b75b'
        );
        checkStatusCode(response, 404);
        checkResponseTime(response, 1);
    });
});