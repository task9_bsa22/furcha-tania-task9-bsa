import {AuthController} from "../lib/controllers/auth.controller";
import {StudentController} from "../lib/controllers/student.controller";
import {checkStatusCode, checkResponseBodyStatus, checkResponseBodyMessage, checkResponseTime} from '../../helpers/functionsForChecking.helper';

const authController = new AuthController();
const studentController = new StudentController();

describe('Using test data for set students settings ', () => {
         let invalidCredentialsDataSet = [
            { avatar: 'avatar', biography: '', company: 'new',  direction: '123', education: '*', employment: '8',
            experience: '/', firstName: 'new', id: 'new', industry: '78', job: 'new job', lastName: 'test', level: '1',  location: '789', role: '', },
            { avatar: 'tania.furcha.97@gmail.com', biography: 'new_biography', company: 'new_company',  direction: 'test78', education: '8', employment: '9',
            experience: '', firstName: 'test', id: '78', industry: '8', job: 'job', lastName: 'lastName', level: '7',  location: 'test_location', role: 'new_role', },
            { avatar: 'new_avatar', biography: 'it is a my biography', company: 'best company',  direction: '8', education: 'high', employment: '-',
            experience: 'no', firstName: 'firstName', id: '25', industry: 'test', job: '56', lastName: 'myLastName', level: 'my_level',  location: 'my_location', role: 'my_role', },
        ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`set settings invalid credentials`, async () => {
            let login = await authController.authenticateUser ('tania.furcha@gmail.com','lytvynuk97');
                
            let accessToken = login.body.accessToken;
            let response = await studentController.setSettingsStudent(accessToken, credentials);
            
            if (response.body.length <= 0) {
                response.body = {};
                response.body.status = "Bad data";
                response.body.message = "Bad credentials";
            }
            checkStatusCode(response, 400);
            checkResponseBodyStatus(response, 'Bad data');
            checkResponseBodyMessage(response, 'Bad credentials');
            checkResponseTime(response, 3000);
        });
    });
});

