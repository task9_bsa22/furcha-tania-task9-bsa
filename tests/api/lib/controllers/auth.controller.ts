import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class AuthController {
    async authenticateUser(emailVal: string, passwordVal: string) {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('POST')
            .url(`auth/login`)
            .body({
                email: emailVal,
                password: passwordVal,
            })
            .send();
        return response;
    }
}
/*for task 9 BSA
const login = 'tania.furcha.97@gmail.com';
const pass = 'lytvynuk97';
export class AuthController {
    async authenticateUser() {
        const response = await new ApiRequest()
        .prefixUrl(baseUrl)
            .method('POST')
            .url(`auth/login`)
            .body({
                email: login,
                password: pass,
            })
            .send();
        return response;
    }
}*/
