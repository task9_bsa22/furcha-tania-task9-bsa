import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class StudentController {
    async setSettingsStudent(accessToken: string, studentObj: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('POST')
            .url(`student`)
            .bearerToken(accessToken)
            .body(studentObj)
            .send();
        return response;
    }

    async getSettingsStudent(accessToken: string, id: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`student?id=${id}`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}

